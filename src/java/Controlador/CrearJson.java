package Controlador;

import DTO.Cliente;
import DTO.Cuenta;
import Negocio.Banco;
import Util.Json;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CrearJson extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            response.setContentType("text/plain");

            String type = request.getParameter("type");
            Banco banco = new Banco();

            switch (type) {
                //Json lista de clientes
                case "clientes":
                    List<Cliente> clientes = banco.encontrarClientes();
                    if (clientes.isEmpty()) 
                        out.print("NO EXISTEN CLIENTES");
                    else
                        out.print(new Json().allClientes(clientes));
                    break;

                //Json lista de cuentas
                case "cuentas":
                    List<Cuenta> cuentas = banco.encontrarCuentas();
                    if (cuentas.isEmpty()) 
                        out.print("NO EXISTEN CUENTAS");
                    else
                        out.print(new Json().allCuentas(cuentas));
                    break;

                //Json para informe por cedula
                case "byCliente":
                        if(request.getParameter("cedula").equals("Seleccionar Cliente")){
                            out.print(request.getParameter("cedula"));
                            return;
                        }
                        int cedula = Integer.parseInt(request.getParameter("cedula"));                                                                        
                        out.print(new Json().getJsonByCedula(cedula));
                    break;

                // Json para informe por cuenta
                case "byCuenta":
                        if(request.getParameter("nrocuenta").equals("Seleccionar Cuenta")){
                            out.print(request.getParameter("nrocuenta"));
                            return;
                        }                        
                        int nrocuenta = Integer.parseInt(request.getParameter("nrocuenta"));    
                        String fecha1 = request.getParameter("fecha1");
                        String fecha2 = request.getParameter("fecha2");
                        out.print(new Json().getJsonByCuenta(nrocuenta, banco,fecha1,fecha2));                          
                    break;

                default:
                    break;
            }
            out.flush();
            out.close();
        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

package Controlador;

import DTO.Cliente;
import Negocio.Banco;
import Util.Json;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ModificarCliente extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            String action = request.getParameter("action");
            Banco banco = new Banco();
            
            //Los Json se generan con la clase Json.java y son enviados por el request
            //para posteriormente obtenerlos en JavaScript

            switch (action) {
                case "busqueda":
                    int cedulafind = Integer.parseInt(request.getParameter("cedulafind"));                    
                    Cliente cliente = banco.encontrarCliente(cedulafind);
                    
                    if (cliente != null) 
                        out.print(new Json().cliente(cliente));
                    else
                        out.print("noexist");
                    break;

                case "modificar":                    
                    int cedula = Integer.parseInt(request.getParameter("cedula"));
                    String nombre = request.getParameter("nombre");
                    String dir = request.getParameter("direccion");
                    String email = request.getParameter("email");
                    String fecha = request.getParameter("fecha");
                    long telefono = Long.parseLong(request.getParameter("telefono"));

                    if (banco.modificarCliente(cedula, nombre, dir, fecha, email, telefono)) {
                        request.getSession().setAttribute("msg", "Modificación Cliente Exitosa");
                        request.getRequestDispatcher("./jsp/Exito/editdeletexitosa.jsp").forward(request, response);
                    } else {
                        request.getSession().setAttribute("error", banco.msgerror);
                        request.getRequestDispatcher("./jsp/error/errorSystem.jsp").forward(request, response);
                    }
                    break;
                    
                case "eliminar":                    
                    int ced = Integer.parseInt(request.getParameter("cedula"));

                    if (banco.eliminarCliente(ced)) {
                        request.getSession().setAttribute("msg", "Eliminación Cliente Exitosa");
                        request.getRequestDispatcher("./jsp/Exito/editdeletexitosa.jsp").forward(request, response);
                    } else {
                        request.getSession().setAttribute("error", banco.msgerror);
                        request.getRequestDispatcher("./jsp/error/errorSystem.jsp").forward(request, response);
                    }
                    break;
                    
                default : break;
            }

        } catch (Exception e) {
            request.getSession().setAttribute("error", e.getMessage());
            request.getRequestDispatcher("./jsp/error/errorSystem.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

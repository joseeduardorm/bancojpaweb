package Controlador;

import DTO.Cuenta;
import Negocio.Banco;
import Util.Json;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ModificarCuenta extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            String action = request.getParameter("action");
            Banco banco = new Banco();

            //Los Json se generan con la clase Json.java y son enviados por el request
            //para posteriormente obtenerlos en JavaScript
            switch (action) {
                case "busqueda":
                    int cuentafind = Integer.parseInt(request.getParameter("cuentafind"));
                    Cuenta cuenta = banco.encontrarCuenta(cuentafind);

                    if (cuenta != null) 
                        out.print(new Json().cuenta(cuenta));
                    else 
                        out.print("noexist");
                    
                    break;

                case "modificar":
                    try {
                        int nroCuenta = Integer.parseInt(request.getParameter("nrocuenta"));
                        int tipo = Integer.parseInt(request.getParameter("tipocuenta"));
                        int cedula = Integer.parseInt(request.getParameter("cedula"));
                        double sobregiro = Double.parseDouble(request.getParameter("sobregiro"));
                        double saldo = Double.parseDouble(request.getParameter("saldo"));
                        
                        if (banco.modificarCuenta(nroCuenta, tipo, sobregiro, cedula, saldo)) {
                            request.getSession().setAttribute("msg", "Modificación Cuenta Exitosa");
                            request.getRequestDispatcher("./jsp/Exito/editdeletexitosa.jsp").forward(request, response);
                        } else {
                            request.getSession().setAttribute("error", banco.msgerror);
                            request.getRequestDispatcher("./jsp/error/errorSystem.jsp").forward(request, response);
                        }
                    }
                    catch(Exception e){
                        request.getSession().setAttribute("error", e.getMessage());
                        request.getRequestDispatcher("./jsp/error/errorSystem.jsp").forward(request, response);
                    }
                    
                    break;

                case "eliminar":

                    int nrocuenta = Integer.parseInt(request.getParameter("nrocuenta"));

                    if (banco.eliminarCuenta(nrocuenta)) {
                        request.getSession().setAttribute("msg", "Eliminación Cuenta Exitosa");
                        request.getRequestDispatcher("./jsp/Exito/editdeletexitosa.jsp").forward(request, response);
                    } else {
                        request.getSession().setAttribute("error", banco.msgerror);
                        request.getRequestDispatcher("./jsp/error/errorSystem.jsp").forward(request, response);
                    }
                    break;

                default:
                    break;
            }

        } catch (Exception e) {
            request.getSession().setAttribute("error", e.getMessage());
            request.getRequestDispatcher("./jsp/error/errorSystem.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

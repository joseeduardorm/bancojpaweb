package Controlador;

import Negocio.Banco;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RegistrarCuenta extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {

            Banco banco = new Banco();            
            int nroCuenta = Integer.parseInt(request.getParameter("nrocuenta"));
            
            if (banco.validarNroCuenta(nroCuenta)) {
                int tipo = Integer.parseInt(request.getParameter("tipocuenta"));
                int cedula = Integer.parseInt(request.getParameter("cedula"));
                Date fecha = java.sql.Date.valueOf(LocalDate.now());                

                if (banco.insertarCuenta(nroCuenta, 0, fecha ,cedula, tipo)) {
                    request.getRequestDispatcher("./jsp/Cuenta/registroexitoso.jsp").forward(request, response);
                } else {
                    request.getSession().setAttribute("error", banco.msgerror);
                    request.getRequestDispatcher("./jsp/error/errorSystem.jsp").forward(request, response);
                }
            } else {
                request.getSession().setAttribute("error", "Nro. Cuenta no válido");
                request.getRequestDispatcher("./jsp/error/errorSystem.jsp").forward(request, response);
            }

        } catch (Exception e) {
            request.getSession().setAttribute("error", e.getMessage());
            request.getRequestDispatcher("./jsp/error/errorSystem.jsp").forward(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

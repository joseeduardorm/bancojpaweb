package Negocio;

import DAO.*;
import DTO.*;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public class Banco {

    public String msgerror = "";
    public final double SOBREGIRO = 1000000;

    public Banco() {
    }

    public boolean insertarCliente(int cedula, String nombre, String dir, String fecha, String email, long telefono) {
        Cliente nuevo = new Cliente();
        nuevo.setCedula(cedula);
        nuevo.setNombre(nombre);
        nuevo.setFechanacimiento(crearFecha(fecha));
        nuevo.setDircorrespondencia(dir);
        nuevo.setTelefono(telefono);
        nuevo.setEmail(email);

        Conexion con = Conexion.getConexion();
        ClienteJpaController clienteDAO = new ClienteJpaController(con.getBd());

        try {
            clienteDAO.create(nuevo);
            return true;
        } catch (Exception ex) {
            msgerror = ex.getMessage();
            return false;
        }
    }

    public boolean modificarCliente(int cedula, String nombre, String dir, String fecha, String email, long telefono) {
        Conexion con = Conexion.getConexion();
        ClienteJpaController clienteDAO = new ClienteJpaController(con.getBd());

        Cliente nuevo = encontrarCliente(cedula);
        nuevo.setNombre(nombre);
        nuevo.setFechanacimiento(crearFecha(fecha));
        nuevo.setDircorrespondencia(dir);
        nuevo.setTelefono(telefono);
        nuevo.setEmail(email);

        try {
            clienteDAO.edit(nuevo);
            return true;
        } catch (Exception ex) {
            msgerror = ex.getMessage();
            return false;
        }

    }

    public boolean eliminarCliente(int cedula) {
        Conexion con = Conexion.getConexion();
        ClienteJpaController clienteDAO = new ClienteJpaController(con.getBd());

        try {
            clienteDAO.destroy(cedula);
            return true;
        } catch (Exception ex) {
            msgerror = ex.getMessage();
            return false;
        }
    }

    public boolean insertarCuenta(int nroCuenta, double saldo, Date fecha, int cedula, int tipo) {
        Conexion con = Conexion.getConexion();
        Cliente nuevo = encontrarCliente(cedula);

        CuentaJpaController cuentaDAO = new CuentaJpaController(con.getBd());
        TipoJpaController tipoDAO = new TipoJpaController(con.getBd());
        Tipo tp = tipoDAO.findTipo(tipo);

        Cuenta nueva = new Cuenta();
        
        //cuenta ahorro tipo=1
        if (tp.getId() == 1) 
            nueva = new Cuenta(nroCuenta, saldo, 0, fecha);
        //cuenta corriente tipo = 2
        else 
            nueva = new Cuenta(nroCuenta, saldo, SOBREGIRO, fecha);
        
        nueva.setCedula(nuevo);
        nueva.setTipo(tp);

        try {
            cuentaDAO.create(nueva);
            return true;
        } catch (Exception ex) {
            msgerror = ex.getMessage();
            return false;
        }
    }

    public boolean modificarCuenta(int nroCuenta, int tipo, double sobregiro, int cedula, double saldo) {
        Conexion con = Conexion.getConexion();
        CuentaJpaController cuentaDAO = new CuentaJpaController(con.getBd());

        Cliente nuevo = encontrarCliente(cedula);
        Cuenta nueva = encontrarCuenta(nroCuenta);
        Tipo tp = encontrarTipo(tipo);

        nueva.setTipo(tp);
        nueva.setSaldo(saldo);
        nueva.setSobregiro(sobregiro);
        nueva.setCedula(nuevo);

        try {
            cuentaDAO.edit(nueva);
            return true;
        } catch (Exception ex) {
            msgerror = ex.getMessage();
            return false;
        }
    }

    public boolean eliminarCuenta(int nrocuenta) {
        Conexion con = Conexion.getConexion();
        CuentaJpaController cuentaDAO = new CuentaJpaController(con.getBd());

        try {
            cuentaDAO.destroy(nrocuenta);
            return true;
        } catch (Exception ex) {
            msgerror = ex.getMessage();
            return false;
        }
    }    

    public boolean insertarMovimiento(int nroCuenta, int valor, int tipo) {
        Conexion con = Conexion.getConexion();
        MovimientoJpaController movDAO = new MovimientoJpaController(con.getBd());

        Cuenta cuenta = encontrarCuenta(nroCuenta);
        TipoMovimiento tipoMov = encontrarTipoMovimiento(tipo);
        java.sql.Date fecha = java.sql.Date.valueOf(LocalDate.now());

        Movimiento mov = new Movimiento();
        mov.setId(movDAO.getMovimientoCount() + 1);
        mov.setIdTipoMovimiento(tipoMov);
        mov.setNroCuenta(cuenta);
        mov.setValor(valor);
        mov.setFecha(fecha);

        //tipo(1) = Consignacion, tipo(2) = Retiro
        try {
            switch (tipo) {
                case 1:
                    if (consignar(cuenta, valor)) {
                        movDAO.create(mov);
                        return true;
                    }
                    break;
                case 2:
                    if (retirar(cuenta, valor)) {
                        movDAO.create(mov);
                        return true;
                    }
            }
            return false;

        } catch (Exception ex) {
            msgerror = ex.getMessage();
            return false;
        }
    }

    private boolean consignar(Cuenta cuenta, int valor) {
        Conexion con = Conexion.getConexion();
        CuentaJpaController cuentaDAO = new CuentaJpaController(con.getBd());

        double valorOperacion = Double.valueOf(valor);
        double saldoDisponible = cuenta.getSaldo();
        double saldoNuevo = saldoDisponible + valorOperacion;
        cuenta.setSaldo(saldoNuevo);
        try {
            cuentaDAO.edit(cuenta);
            return true;

        } catch (Exception ex) {
            msgerror = ex.getMessage();
            return false;
        }
    }

    private boolean retirar(Cuenta cuenta, int valor) {        
        if (cuenta.getTipo().getId() == 1) {
            return retirarAhorro(cuenta, valor);
        } else if (cuenta.getTipo().getId() == 2) {
            return retirarCorriente(cuenta, valor);
        }
        return false;
    }

    private boolean retirarAhorro(Cuenta cuenta, int valor) {
        Conexion con = Conexion.getConexion();
        CuentaJpaController cuentaDAO = new CuentaJpaController(con.getBd());

        double saldoDisponible = cuenta.getSaldo();
        double valorOperacion = Double.valueOf(valor);

        if (saldoDisponible >= valorOperacion) {
            double saldoNuevo = saldoDisponible - valorOperacion;
            cuenta.setSaldo(saldoNuevo);

            try {
                cuentaDAO.edit(cuenta);
                return true;
            } catch (Exception ex) {
                msgerror = ex.getMessage();
                return false;
            }
        }
        return false;
    }

    private boolean retirarCorriente(Cuenta cuenta, int valor) {
        Conexion con = Conexion.getConexion();
        CuentaJpaController cuentaDAO = new CuentaJpaController(con.getBd());

        double sobregiro = cuenta.getSobregiro();
        double saldoDisponible = cuenta.getSaldo();
        double valorOperacion = Double.valueOf(valor);

        if (saldoDisponible + sobregiro >= valorOperacion) {
            double saldoNuevo = saldoDisponible - valorOperacion;
            cuenta.setSaldo(saldoNuevo);

            try {
                cuentaDAO.edit(cuenta);
                return true;
            } catch (Exception ex) {
                msgerror = ex.getMessage();
                return false;
            }
        }
        return false;
    }

    public boolean insertarMovimientoTransferencia(int nroCuenta, int nroCuentaDestino, int valor) {
        Conexion con = Conexion.getConexion();
        MovimientoJpaController movDAO = new MovimientoJpaController(con.getBd());

        Cuenta cuenta = encontrarCuenta(nroCuenta);
        Cuenta cuentaDestino = encontrarCuenta(nroCuentaDestino);
        java.sql.Date fecha = java.sql.Date.valueOf(LocalDate.now());

        //Movimiento retiro
        Movimiento mov = new Movimiento();
        mov.setId(movDAO.getMovimientoCount() + 1);
        mov.setIdTipoMovimiento(encontrarTipoMovimiento(2));
        mov.setNroCuenta(cuenta);
        mov.setValor(valor);
        mov.setFecha(fecha);

        //Movimiento consignacion
        Movimiento mov2 = new Movimiento();
        mov2.setId(movDAO.getMovimientoCount() + 1);
        mov2.setIdTipoMovimiento(encontrarTipoMovimiento(1));
        mov2.setNroCuenta(cuentaDestino);
        mov2.setValor(valor);
        mov2.setFecha(fecha);

        try {
            //si el retiro es válido, hace la consignación.
            if (retirar(cuenta, valor)) {
                movDAO.create(mov);
                consignar(cuentaDestino, valor);
                movDAO.create(mov2);
                return true;
            }
            return false;

        } catch (Exception ex) {
            msgerror = ex.getMessage();
            return false;
        }        
    }
    
    public List<Cliente> encontrarClientes() {
        Conexion con = Conexion.getConexion();
        ClienteJpaController clienteDAO = new ClienteJpaController(con.getBd());
        
        return clienteDAO.findClienteEntities();
    }

    public Cliente encontrarCliente(int cedula) {
        Conexion con = Conexion.getConexion();
        ClienteJpaController clienteDAO = new ClienteJpaController(con.getBd());
        
        return clienteDAO.findCliente(cedula);
    }

    public List<Cuenta> encontrarCuentas() {
        Conexion con = Conexion.getConexion();
        CuentaJpaController cuentaDAO = new CuentaJpaController(con.getBd());

        return cuentaDAO.findCuentaEntities();
    }

    public Cuenta encontrarCuenta(int nrocuenta) {
        Conexion con = Conexion.getConexion();
        CuentaJpaController cuentaDAO = new CuentaJpaController(con.getBd());
        
        return cuentaDAO.findCuenta(nrocuenta);
    }

    public Tipo encontrarTipo(int tipo) {
        Conexion con = Conexion.getConexion();
        TipoJpaController tipoDAO = new TipoJpaController(con.getBd());
        
        return tipoDAO.findTipo(tipo);
    }

    public TipoMovimiento encontrarTipoMovimiento(int tipo) {
        Conexion con = Conexion.getConexion();
        TipoMovimientoJpaController tipoMovDAO = new TipoMovimientoJpaController(con.getBd());
        
        return tipoMovDAO.findTipoMovimiento(tipo);
    }

    public Usuario encontrarUsusario(String user) {
        Conexion con = Conexion.getConexion();
        UsuarioJpaController userDAO = new UsuarioJpaController(con.getBd());
        
        return userDAO.findUsuario(user);
    }

    public boolean validarUsuario(Usuario usuario, String contraseña) {
        if (usuario == null) 
            return false;
        
        return usuario.getContraseña().equals(contraseña);
    }

    private Date crearFecha(String fecha) {
        String fechas[] = fecha.split("-");
        int agno = Integer.parseInt(fechas[0]);
        int mes = Integer.parseInt(fechas[1]);
        int dia = Integer.parseInt(fechas[2]);        
        LocalDate date = LocalDate.of(agno, mes, dia);
        
        return java.sql.Date.valueOf(date);
    }

    public boolean validarNroCuenta(int nrocuenta) {
        BigInteger ncuenta = new BigInteger(nrocuenta + "");
        BigInteger min = new BigInteger("100000000");
        BigInteger max = new BigInteger("999999999");

        int res1 = min.compareTo(ncuenta);//<0
        int res2 = max.compareTo(ncuenta);//>0

        return (res1 < 0 && res2 > 0);
    }

}

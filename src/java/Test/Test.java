package Test;

import DAO.ClienteJpaController;
import DAO.Conexion;
import DTO.Cliente;
import java.util.Date;
import java.util.List;

public class Test {
    
    
    public static void main(String[] args) {
        //Instancia una conexion con la BD
        Conexion con=Conexion.getConexion();    
        
        //Instancia controlador JPA para cliente
        ClienteJpaController clienteDAO = new ClienteJpaController(con.getBd());
        
        //Obtiente lista de clientes de la BD.
        List<Cliente> clientes=clienteDAO.findClienteEntities();
        
        //Itera los clientes de la BD
        clientes.forEach((dato) -> {
            System.out.println(dato.toString());
        });
        
        //Creacion cliente nuevo        
        Cliente nuevo=new Cliente(123,"ANGEL ",new Date(3919,12,30),"DIRECCION EVENTO 1",350,"angel.vivas94@gmail.com");
        try {
            //Crea el nuevo cliente en la BD
            clienteDAO.create(nuevo);
            System.out.println("Se ha creado con éxito:"+nuevo.toString());
        } catch (Exception ex) {
            //Logger.getLogger(TestConexion.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println(ex.getMessage());
        }
    }    
}

package Util;

import DTO.*;
import Negocio.Banco;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class Json {

    public JSONObject cliente(Cliente cliente) {

        JSONObject obj = new JSONObject();
        obj.put("cedula", cliente.getCedula());
        obj.put("nombre", cliente.getNombre());
        obj.put("direccion", cliente.getDircorrespondencia());
        obj.put("email", cliente.getEmail());
        obj.put("telefono", cliente.getTelefono());
        obj.put("fecha", cliente.getFechanacimiento());

        return obj;
    }

    public JSONObject allClientes(List<Cliente> clientes) {
        JSONObject obj = new JSONObject();
        JSONArray list = new JSONArray();

        for (Cliente cliente : clientes) {
            JSONObject obj2 = cliente(cliente);
            list.put(obj2);
        }

        obj.put("cliente", list);
        return obj;
    }

    public JSONObject cuenta(Cuenta cuenta) {

        JSONObject obj = new JSONObject();
        obj.put("nrocuenta", cuenta.getNroCuenta());
        obj.put("saldo", cuenta.getSaldo());
        obj.put("sobregiro", cuenta.getSobregiro());
        obj.put("fechacrecion", cuenta.getFechacreacion());
        obj.put("cliente", cuenta.getCedula().getCedula() + " | " + cuenta.getCedula().getNombre());
        obj.put("tipo", cuenta.getTipo().getNombre());

        return obj;
    }

    public JSONObject allCuentas(List<Cuenta> cuentas) {
        JSONObject obj = new JSONObject();
        JSONArray list = new JSONArray();

        for (Cuenta cuenta : cuentas) {
            JSONObject obj2 = cuenta(cuenta);
            list.put(obj2);
        }

        obj.put("cuenta", list);
        return obj;
    }

    public JSONObject getJsonByCedula(int cedula) {
        Banco banco = new Banco();
        Cliente cliente = banco.encontrarCliente(cedula);
        JSONObject obj = new JSONObject();
        JSONArray list = new JSONArray();

        obj.put("cedula", cliente.getCedula());
        obj.put("nombre", cliente.getNombre());
        obj.put("fechaNacimiento", cliente.getFechanacimiento());
        obj.put("dirCorrespondencia", cliente.getDircorrespondencia());
        obj.put("telefono", cliente.getTelefono());
        obj.put("email", cliente.getEmail());
        list = getJsonCuentas(cliente.getCuentaList());
        obj.put("cuentas", list);
        return obj;
    }

    public JSONObject getJsonByCuenta(int nrocuenta, Banco banco, String fecha1, String fecha2) {
        JSONObject obj = new JSONObject();
        Cuenta cuenta = banco.encontrarCuenta(nrocuenta);
        obj.put("cedula", cuenta.getCedula().getCedula());
        obj.put("cuenta", cuenta.getNroCuenta());
        obj.put("nombre", cuenta.getCedula().getNombre());
        obj.put("saldo", cuenta.getSaldo());
        obj.put("operaciones", this.getJsonOperaciones(cuenta.getMovimientoList(), fecha1, fecha2));
        return obj;
    }

    private JSONArray getJsonCuentas(List<Cuenta> cuentas) {
        JSONArray list = new JSONArray();

        for (Cuenta cuenta : cuentas) {

            JSONObject obj = new JSONObject();
            obj.put("numero", cuenta.getNroCuenta());
            obj.put("tipo", cuenta.getTipo().getNombre());
            obj.put("saldo", cuenta.getSaldo());
            JSONArray list2 = this.getJsonOperaciones(cuenta.getMovimientoList(),"", "");
            obj.put("operaciones", list2);
            list.put(obj);

        }
        return list;
    }

    private JSONArray getJsonOperaciones(List<Movimiento> movimientos, String fecha1, String fecha2) {
        JSONArray list = new JSONArray();

        if (movimientos != null) {
            for (Movimiento movimiento : movimientos) {  

                JSONObject obj = new JSONObject();
                obj.put("id", movimiento.getId());
                obj.put("tipo", movimiento.getIdTipoMovimiento().getId());
                obj.put("valor", movimiento.getValor());
                obj.put("fecha", movimiento.getFecha());
                if (fecha1.isEmpty() && fecha2.isEmpty()) {
                    list.put(obj);
                } else {
                    Date fecha = movimiento.getFecha();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        Date date1 = sdf.parse(fecha1);
                        Date date2 = sdf2.parse(fecha2);
                        if (date1.before(fecha) && date2.after(fecha)) {
                            list.put(obj);
                        }
                    } catch (ParseException ex) {
                        Logger.getLogger(Json.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        } else {
            JSONObject obj = new JSONObject();
            obj.put("id", 2248);
            list.put(obj);
        }
        return list;
    }
}

var request = new XMLHttpRequest();

function pulsar(e) {
  var tecla = (document.all) ? e.keyCode :e.which;  
  if(tecla==13) loadCliente();
}

function loadCliente() {    
    request.open("POST", "../../ModificarCliente.do", true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            var view = document.getElementById('view').value;            
            var obj = request.responseText;
            
            if(view === "edit"){
               if(obj === "noexist"){
                   $(".loader").fadeOut("slow");
                   alert("ERROR, CEDULA NO REGISTRADA!");
                   var div = "<div id='dataspace'></div>";
                   document.getElementById("dataspace").innerHTML = div;
               }
               else{                   
                   createeditform(obj);
                   $(".loader").fadeOut("slow");
               }
            }
            else if(view === "delete"){
                if(obj === "noexist"){
                   $(".loader").fadeOut("slow");
                   alert("ERROR, CEDULA NO REGISTRADA!");
                   var div = "<div id='dataspace'></div>";
                   document.getElementById("dataspace").innerHTML = div;
               }
               else{
                   createdeleteform(obj); 
                   $(".loader").fadeOut("slow");
               }
            }

        } else if (request.readyState == 4 && request.status != 200) {
            alert(obj);
            $(".loader").fadeOut("slow");
        }
    };
    var cedulafind = document.getElementById('cedulafind').value;
    if(cedulafind===""){
        alert("Digite número de cédula");
        return;
    }
    request.send("action=busqueda&cedulafind=" + cedulafind);
    document.getElementById("divloader").innerHTML = "<div class='loader'></div>";
}

function createeditform(obj){
    var json = JSON.parse(obj);
    
    var form = "<h3 class='text-center'> Cambiar datos </h3>";
    form += "<form onsubmit='loader()' name='registrar' action='../../ModificarCliente.do' class='register'>";    
    form += "<input type='hidden' name='cedula' value='"+ json.cedula +"' >";
    form += "<p>Nombre:<input class='register-input' type='text' name='nombre' value='"+ json.nombre +"' required></p>";
    form += "<p>E-mail:<input class='register-input'  type='email' name='email' value='"+ json.email +"' required></p>";    
    form += "<p>Direccion:<input class='register-input'  type='text' name='direccion' value='"+ json.direccion +"' required></p>";
    form += "<p>Teléfono:<input class='register-input'  type='number' name='telefono' value='"+ json.telefono +"' required></p>";    
    form += "<p>Fecha de Nacimiento:<input class='register-input'  type='date' name='fecha' value='"+ json.fecha +"' required></p>";
    form += "<input type='hidden' value='modificar' name='action'>";
    form += "<input type='submit'  value='Modificar' name='modificar' class='register-button' >";
    form += "</form>";
    
    document.getElementById("dataspace").innerHTML = form;    
    document.getElementById("lnk").click();
}

function createdeleteform(obj){
    var json = JSON.parse(obj);
    var n = json.nombre;
    
    var form = "<h3 class='text-center'> Datos Cliente </h3>";
    form += "<form onsubmit='loader()' name='registrar' action='../../ModificarCliente.do' class='register'>";    
    form += "<input type='hidden' name='cedula' value='"+ json.cedula +"' >";
    form += "<p>Nombre:<input class='register-input' type='text' name='nombre' value='"+ json.nombre +"' disabled></p>";
    form += "<p>E-mail:<input class='register-input'  type='email' name='email' value='"+ json.email +"' disabled></p>";    
    form += "<p>Direccion:<input class='register-input'  type='text' name='direccion' value='"+ json.direccion +"' disabled></p>";
    form += "<p>Teléfono:<input class='register-input'  type='number' name='telefono' value='"+ json.telefono +"' disabled></p>";    
    form += "<p>Fecha de Nacimiento:<input class='register-input'  type='date' name='fecha' value='"+ json.fecha +"' disabled></p>";
    form += "<input type='hidden' value='eliminar' name='action'>";
    form += "<input type='submit' value='Eliminar' name='eliminar' class='register-button' >";
    form += "</form>";
    
    document.getElementById("dataspace").innerHTML = form;
    document.getElementById("lnk").click();    
}

function loader(){
    document.getElementById("divloader").innerHTML = "<div class='loader'></div>";
}

function maxL(object){
    if (object.value.length > object.maxLength) 
        object.value = object.value.slice(0, object.maxLength);
}
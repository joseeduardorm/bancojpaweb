
var request = new XMLHttpRequest();
var titulo = "";
var tabl = "";
var nombre = "";

function loadListClientes() {
    request.open("POST", "../../CrearJson.do", true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            var obj = request.responseText;
            if (obj === "NO EXISTEN CLIENTES") {
                alert(obj);
                return;
            }

            var j = JSON.parse(obj);
            var select = "<select name='cedula'>";
            select += "<option>Seleccionar Cliente</option>";
            for (var i = 0; i < j.cliente.length; i++) {
                var cedula = j.cliente[i].cedula;
                var nombre = j.cliente[i].nombre;
                select += "<option value=" + cedula + ">" + cedula + " | " + nombre + "</option>";
            }

            document.getElementById('selectCliente').innerHTML = select;
            $(".loader").fadeOut("slow");
        } else if (request.readyState == 4 && request.status != 200) {
            var message = request.responseText;
            message = request.responseText;
            alert(message);
        }
    };
    request.send("type=clientes");
    document.getElementById("divloader").innerHTML = "<div class='loader'></div>";
}

function loadListCuentas() {
    request.open("POST", "../../CrearJson.do", true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            var obj = request.responseText;
            if (obj === "NO EXISTEN CUENTAS") {
                alert(obj);
                return;
            }

            var j = JSON.parse(obj);
            var select = "<select name='nrocuenta'>";
            select += "<option>Seleccionar Cuenta </option>";
            for (var i = 0; i < j.cuenta.length; i++) {
                var cuenta = j.cuenta[i].nrocuenta;
                var tipo = j.cuenta[i].tipo;
                select += "<option value=" + cuenta + ">" + cuenta + " | " + tipo + "</option>";
            }

            document.getElementById('selectCuenta').innerHTML = select;
            $(".loader").fadeOut("slow");;
            
        } else if (request.readyState == 4 && request.status != 200) {
            var message = request.responseText;
            message = request.responseText;
            alert(message);
        }
    };
    request.send("type=cuentas");
    document.getElementById("divloader").innerHTML = "<div class='loader'></div>";
}

function loadListCuentas2() {
    request.open("POST", "../../CrearJson.do", true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            var obj = request.responseText;
            if (obj === "NO EXISTEN CUENTAS") {
                alert(obj);
                return;
            }

            var j = JSON.parse(obj);
            var select = "<select name='nrocuenta'>";
            select += "<option>Seleccionar Cuenta</option>";
            for (var i = 0; i < j.cuenta.length; i++) {
                var cuenta = j.cuenta[i].nrocuenta;
                var tipo = j.cuenta[i].tipo;
                select += "<option value=" + cuenta + ">" + cuenta + " | " + tipo + "</option>";
            }

            var select2 = "<select name='nrocuentadestino'>";
            select2 += "<option>Seleccionar Cuenta</option>";
            for (var k = 0; k < j.cuenta.length; k++) {
                var cuenta2 = j.cuenta[k].nrocuenta;
                var tipo2 = j.cuenta[k].tipo;
                select2 += "<option value=" + cuenta2 + ">" + cuenta2 + " | " + tipo2 + "</option>";
            }

            document.getElementById('selectCuenta').innerHTML = select;
            document.getElementById('selectCuenta2').innerHTML = select2;
            $(".loader").fadeOut("slow");
        } else if (request.readyState == 4 && request.status != 200) {
            var message = request.responseText;
            message = request.responseText;
            alert(message);
        }
    };
    request.send("type=cuentas");
    document.getElementById("divloader").innerHTML = "<div class='loader'></div>";
}

function loadJsonCliente() {
    request.open("POST", "../../CrearJson.do", true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    request.onreadystatechange = function () {
        // si la respuesta fue exitosa
        if (request.readyState == 4 && request.status == 200) {

            var obj = request.responseText;
            if (obj === "NO HAY BANCO CREADO") {
                alert(obj);
                return;
            }
            if (obj === "Seleccionar Cliente") {
                alert(obj);
                return;
            }

            //Desplegando JSON en tabla html
            var message = request.responseText;
            crearTablaCliente(message);
            $(".loader").fadeOut("slow");

        } else if (request.readyState == 4 && request.status != 200) {
            var message = request.responseText;
            message = request.responseText;
            alert(message);
        }
    };
    var cedula = document.getElementById('selectCliente').value;
    request.send("type=byCliente&cedula=" + cedula);
    document.getElementById("divloader").innerHTML = "<div class='loader'></div>";
}

function loadJsonCuenta() {
    
    request.open("POST", "../../CrearJson.do", true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    request.onreadystatechange = function () {

        // si la respuesta fue exitosa
        if (request.readyState == 4 && request.status == 200) {
            var obj = request.responseText;            
            if (obj === "Seleccionar Cuenta") {
                alert(obj);
                return;
            }

            //Desplegando JSON en tabla html
            var message = request.responseText;
            crearTablaCuenta(message);
            $(".loader").fadeOut("slow");

        } else if (request.readyState == 4 && request.status != 200) {
            var message = request.responseText;
            message = request.responseText;
            alert(message);
        }
    };
    var cuenta = document.getElementById('selectCuenta').value;
    var fe1 = document.getElementById('fecha1').value;
    var fe2 = document.getElementById('fecha2').value;
    request.send("type=byCuenta&nrocuenta=" + cuenta + "&fecha1=" + fe1 + "&fecha2=" + fe2);
    document.getElementById("divloader").innerHTML = "<div class='loader'></div>";

}

function crearTablaCliente(message) {
    var j = JSON.parse(message);
    var tit = "";
    var pdfescritura = "";
    var tabla = "<br><br><label> ";
    tabla += ("INFORME DEL BANCO MIS AHORROS: SEÑOR(A) :</label>");
    tabla += ("<label>" + j.nombre + " CON CEDULA NUMERO: " + j.cedula);
    tabla += (" FECHA DE NACIMIENTO : " + j.fechaNacimiento + " DIRECCION: " + j.dirCorrespondencia);
    tabla += (" TELEFONO : " + j.telefono + " EMAIL: " + j.email);
    tabla += ("</label>");
    tabla += ("<hr>");

    tit += "INFORME DEL BANCO MIS AHORROS \n SEÑOR(A) : " + j.nombre + " CON CEDULA NUMERO: " + j.cedula +
            "\n FECHA DE NACIMIENTO : " + j.fechaNacimiento + "\n DIRECCION: " + j.dirCorrespondencia +
            " TELEFONO : " + j.telefono + "\n EMAIL: " + j.email;



    tabla += ("<table>");

    tabla += ("<tr>");
    tabla += ("<th>  CUENTAS </th>");
    tabla += ("</tr>");

    tabla += ("<tr>");
    tabla += ("<th> NRO. DE CUENTA </th>");
    tabla += ("<th> TIPO DE CUENTA </th>");
    tabla += ("<th> SALDO </th>");
    tabla += ("<th> OPERACIONES </th>");
    tabla += ("</tr>");
    pdfescritura += "CUENTAS \n";
    for (var i = 0; i < j.cuentas.length; i++) {
        pdfescritura += " \n\n NRO. DE CUENTA   TIPO DE CUENTA   SALDO  \n";
        pdfescritura += j.cuentas[i].numero + "                                 " + j.cuentas[i].tipo + "                     " + j.cuentas[i].saldo;
        tabla += ("<tr>");
        tabla += ("<td> " + j.cuentas[i].numero + "</td>");
        tabla += ("<td> " + j.cuentas[i].tipo + "</td>");
        if (j.cuentas[i].saldo < 0)
            tabla += ("<td style='color:red;'> " + j.cuentas[i].saldo + "</td>");
        else
            tabla += ("<td> " + j.cuentas[i].saldo + "</td>");
        tabla += ('<td> <input type"button" type="button" class="btn btn-block btn-primary btn-sm dropdown-toggle" id="ope' + i + '" onclick= "ver(' + i + ')" value= "Mostrar"> </td>');
        tabla += ("</tr>");

        tabla += ("<tr id = 'divv" + i + "'style='display: none'>");
        tabla += ("<td>");

        tabla += ("<table class='toper'>");
        tabla += ("<tr>");
        tabla += ("<th>  id </th>");
        tabla += ("<th>  tipo </th>");
        tabla += ("<th>  valor </th>");
        tabla += ("<th>  Saldo </th>");
        tabla += ("<th>  fecha </th>");

        tabla += ("</tr>");
        pdfescritura += "Operaciones \n ID  TIPO                  VALOR   SALDO  FECHA   \n";
        var saldoActual = 0;
        for (var jj = 0; jj < j.cuentas[i].operaciones.length; jj++) {
            var valor = j.cuentas[i].operaciones[jj].valor;

            pdfescritura += j.cuentas[i].operaciones[jj].id + "       ";

            tabla += ("<tr>");
            tabla += ("<td>" + j.cuentas[i].operaciones[jj].id + "</td>");
            if (j.cuentas[i].operaciones[jj].tipo === 1) {
                tabla += ("<td> Consignación</td>");
                pdfescritura += "Consignación";
            } else {
                tabla += ("<td> Retiro </td>");
                pdfescritura += "Retiro             ";
            }

            if (j.cuentas[i].operaciones[jj].tipo === 2) {
                tabla += ("<td style='color:red;'>" + "-" + j.cuentas[i].operaciones[jj].valor + "</td>");
                valor = j.cuentas[i].operaciones[jj].valor * -1;
            } else {
                tabla += ("<td>" + "+" + j.cuentas[i].operaciones[jj].valor + "</td>");
            }

            saldoActual = saldoActual + valor;
            if (saldoActual < 0)
                tabla += ("<td style='color:red;'>" + saldoActual + "</td>");
            else
                tabla += ("<td>" + saldoActual + "</td>");


            pdfescritura += j.cuentas[i].operaciones[jj].valor;


            tabla += ("<td>" + j.cuentas[i].operaciones[jj].fecha + "</td>");
            pdfescritura += "    " + j.cuentas[i].operaciones[jj].saldoActual + "      " + j.cuentas[i].operaciones[jj].fecha + "\n";

            tabla += ("</tr>");
        }

        tabla += ("</table>");
        tabla += ("<td> ");
        tabla += ("</tr>");
    }
    tabla += ("</table>");
    titulo = tit;
    tabl = pdfescritura;
    nombre = "informe por cliente.pdf";

    document.getElementById("space").innerHTML = tabla;
    document.getElementById("myfooter").style.position = "relative";
}

function crearTablaCuenta(message) {
    var j = JSON.parse(message);
    var tabla = "<br><br><label> ";
    var pdfescritura = "";
    tabla += ("INFORME DEL BANCO MIS AHORROS POR CUENTA:</label>");
    tabla += ("<br> <label> PERTENECE A SEÑOR(A): " + j.nombre + " CON CEDULA NUMERO: " + j.cedula);
    tabla += ("</label>");
    tabla += ("<hr>");
    if (j.saldo < 0)
        tabla += ("<h4 style='color:red;'> SALDO : " + j.saldo + "</h4>");
    else
        tabla += ("<h4> SALDO : " + j.saldo + "</h4>");


    tabla += ("<table>");
    tabla += ("<tr>");
    tabla += ("<td> OPERACIONES </td>");
    tabla += ("</tr>");

    pdfescritura += "Operaciones \n ID  TIPO                  VALOR   SALDO  FECHA   \n";
    tabla += ("<tr>");
    tabla += ("<td> ID </td>");
    tabla += ("<td> TIPO </td>");
    tabla += ("<td> VALOR </td>");
    tabla += ("<td> SALDO</td>");
    tabla += ("<td> FECHA </td>");
    tabla += ("</tr>");

    var saldoActual = 0;
    for (var i = 0; i < j.operaciones.length; i++) {
        var valor = j.operaciones[i].valor;

        tabla += ("<tr>");
        tabla += ("<td> " + j.operaciones[i].id + "</td>");
        pdfescritura += j.operaciones[i].id + "     ";
        if (j.operaciones[i].tipo === 1) {
            tabla += ("<td> Consignación</td>");
            pdfescritura += "Consignación";
        } else {
            tabla += ("<td> Retiro </td>");
            pdfescritura += "Retiro             ";
        }



        if (j.operaciones[i].tipo === 2) {
            tabla += ("<td style='color:red;'>" + " - " + j.operaciones[i].valor + "</td>");
            pdfescritura += j.operaciones[i].valor + "          ";
            valor = j.operaciones[i].valor * -1;
        } else {
            tabla += ("<td>" + "+" + j.operaciones[i].valor + "</td>");
            pdfescritura += j.operaciones[i].valor + "         ";
        }
        saldoActual = saldoActual + valor;
        if (saldoActual < 0)
            tabla += ("<td style='color:red;'>-" + saldoActual + "</td>");
        else
            tabla += ("<td>" + saldoActual + "</td>");



        pdfescritura += saldoActual + "         ";
        tabla += ("<td> " + j.operaciones[i].fecha + "</td>");
        pdfescritura += j.operaciones[i].fecha + " \n";
        tabla += ("</tr>");

    }

    titulo = "INFORME POR CUENTA \n " + "CUENTA NUMERO " + j.cuenta + "\n PERTENECE A: " +
            j.nombre + "\n CON CEDULA NUMERO:" + j.cedula + "";
    tabl = pdfescritura;
    nombre = "informe cuenta.pdf";

    document.getElementById("space2").innerHTML = tabla;
    document.getElementById("myfooter").style.position = "relative";
}

function ver(a) {
    var direccion = "divv" + a;
    var nbot = "ope" + a;
    var boton = document.getElementById(direccion);
    if (boton.style.display === "none") {
        document.getElementById(nbot).value = "Ocultar";
        boton.style.display = "block";
    } else {
        document.getElementById(nbot).value = "Mostar";
        boton.style.display = "none";
    }
}

function generarpdf() {
    var doc = new jsPDF();

    doc.text(20, 25, "INFORME");
    doc.text(20, 45, titulo);
    doc.setLineWidth(1.5);
    doc.line(20, 80, 200, 80);
    doc.text(20, 90, tabl);
    doc.setLineWidth(1.5);
    doc.line(20, 35, 200, 35);

    pageHeight = doc.internal.pageSize.height;

// Before adding new content
    y = 500 // Height position of new content
    if (y >= pageHeight)
    {
        doc.addPage();
        y = 0 // Restart height position
    }
    doc.text(20, 25, "prueba" + pageHeight);

    doc.save(nombre);
}

function loader(){
    document.getElementById("divloader").innerHTML = "<div class='loader'></div>";
}

function maxL(object){
    if (object.value.length > object.maxLength) 
        object.value = object.value.slice(0, object.maxLength);
}
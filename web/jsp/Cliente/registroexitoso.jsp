<%@page import="DAO.Conexion"%>
<%@page import="DAO.ClienteJpaController"%>
<%@page import="DTO.Cliente"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="./css/estilo.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="shortcut icon" href="./imagenes/fav.ico" />
        <title>Registro Exitoso Clientes</title>
    </head>
    
    <body>
        <!––  linea de banner--> 
        <header  id="banner" >
            <div class="row"  >
                <div class="col-lg-2">
                    &nbsp <a href="../../CerrarSesion.do" class="btn btn-primary btn-md active" id="btnlogin" role="button" aria-pressed="true">LogOut</a>
                </div>
                <div class="col-lg-7">
                    <h1 class="text-center" >BANCO MIS AHORROS</h1>
                </div>
                <div class="col-lg-1">
                    <img src="./imagenes/face.png" class="img-fluid" alt="Responsive image">
                </div>
                <div class="col-lg-1">
                    <img src="./imagenes/twiter.png" class="img-fluid" alt="Responsive image">
                </div>
                <div class="col-lg-1">
                    <img src="./imagenes/ins.png" class="img-fluid" alt="Responsive image">
                </div>
            </div>
        </header>

        <aside id="contenido">
            <!––  botones--> 
            <div class="container" >
                <div class="row" >
                    <div class= "col-md-4"   >
                        <div class="container">
                            <a class="btn btn-primary btn-lg btn-block" href="./index.html" role="button" id="inicio">Inicio</a>
                        </div>
                    </div>
                    
                    <div class= "col-md-4" >
                        <div class= "container">
                            <a class="btn btn-primary btn-lg btn-block" href="./jsp/Cliente/registrarCliente.jsp" role="button" id="otro">Ingresar otro cliente </a>
                        </div>
                    </div>

                    <div class= "col-md-4 " >
                        <div class="container" >
                            <a class="btn btn-primary btn-lg btn-block" href="#" role="button" > Contactanos</a>
                        </div>
                    </div>
                </div> 
            </div>
        </aside>

        <h1 class="register-title">Registro de Cliente Exitoso</h1>
        <br>
        <hr>
        <% 
            Conexion con=Conexion.getConexion();
            ClienteJpaController clienteDAO = new ClienteJpaController(con.getBd());        
            List<Cliente> clientes=clienteDAO.findClienteEntities();
            for(Cliente dato:clientes){         
        %>
        <p><%= dato.toString()%></p>        
        <% } %>
        <hr>
    </body>
</html>

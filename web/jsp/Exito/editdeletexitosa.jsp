
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="./css/estilo.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="shortcut icon" href="./imagenes/fav.ico" />
        <%
            String message = (String) (request.getSession().getAttribute("msg"));
        %>
        <title><%= message %></title>
    </head>
    
    <body>
        <!––  linea de banner--> 
        <header  id="banner" >
            <div class="row"  >
                <div class="col-lg-9">
                    <h1 class="text-center" >BANCO MIS AHORROS</h1>
                </div>
                <div class="col-lg-1">
                    <img src="./imagenes/face.png" class="img-fluid" alt="Responsive image">
                </div>
                <div class="col-lg-1">
                    <img src="./imagenes/twiter.png" class="img-fluid" alt="Responsive image">
                </div>
                <div class="col-lg-1">
                    <img src="./imagenes/ins.png" class="img-fluid" alt="Responsive image">
                </div>
            </div>
        </header>

        <aside id="contenido">
            <!––  botones--> 
            <div class="container" >
                <div class="row" >
                    <div class= "col-md-2">
                    </div>
                    
                    <div class= "col-md-4"   >
                        <div class="container">
                            <a class="btn btn-primary btn-lg btn-block" href="./index.html" role="button" id="inicio">Inicio</a>
                        </div>
                    </div>
                    
                    <div class= "col-md-4 " >
                        <div class="container" >
                            <a class="btn btn-primary btn-lg btn-block" href="#" role="button"> Contactanos</a>
                        </div>
                    </div>
                    
                    <div class= "col-md-2">
                    </div>
                </div> 
            </div>
        </aside>

        <h1 class="register-title"><%= message %></h1>
        <br>
        <hr>
    </body>
</html>
